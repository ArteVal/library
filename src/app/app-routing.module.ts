import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BookListComponent } from './components/book-list/book-list.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BookEditComponent } from './components/book-edit/book-edit.component';
import { AuthorListComponent } from './components/author-list/author-list.component';
import { AuthorDetailComponent } from './components/author-detail/author-detail.component';
import { AuthorEditComponent } from './components/author-edit/author-edit.component';

const routes: Routes = [
  { path: '', redirectTo: '/books', pathMatch: 'full' },
  { path: 'books',  component: BookListComponent },
  { path: 'book/:id', component: BookDetailComponent },
  { path: 'book/edit/:id', component: BookEditComponent },
  { path: 'authors',     component: AuthorListComponent },
  { path: 'author/:id',     component: AuthorDetailComponent },
  { path: 'author/edit/:id',     component: AuthorEditComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
