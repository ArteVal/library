import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { PopoverModule, PaginationModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BookEditComponent } from './components/book-edit/book-edit.component';
import { AuthorListComponent } from './components/author-list/author-list.component';
import { AuthorDetailComponent } from './components/author-detail/author-detail.component';
import { AuthorEditComponent } from './components/author-edit/author-edit.component';

import { UserService } from './services/user.service';
import { BookService } from './services/book.service';
import { AuthorService } from './services/author.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BookListComponent,
    BookDetailComponent,
    BookEditComponent,
    AuthorEditComponent,
    AuthorListComponent,
    AuthorDetailComponent,
  ],
  imports: [
    BrowserModule,
    PopoverModule.forRoot(),
    PaginationModule.forRoot(),
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    UserService,
    BookService,
    AuthorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
