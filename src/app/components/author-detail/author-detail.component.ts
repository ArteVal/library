import { Component, OnInit } from '@angular/core';
import { AuthorService} from '../../services/author.service';
import { Author } from '../../interfaces/author';

import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
@Component({
  selector: 'app-author-detail',
  templateUrl: './author-detail.component.html',
  styleUrls: ['./author-detail.component.css']
})
export class AuthorDetailComponent implements OnInit {

  public author: Author;
  constructor(
    private authorService: AuthorService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit() {
    this.getAuthor();
  }
  private getAuthor(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.authorService.getAuthor(+params.get('id')))
      .subscribe(author => this.author = author);
  }
}
