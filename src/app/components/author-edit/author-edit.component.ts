import { Component, OnInit } from '@angular/core';
import { AuthorService} from '../../services/author.service';
import { Author } from '../../interfaces/author';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
@Component({
  selector: 'app-author-edit',
  templateUrl: './author-edit.component.html',
  styleUrls: ['./author-edit.component.css']
})
export class AuthorEditComponent implements OnInit {

  public author: Author;
  constructor(
    private authorService: AuthorService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    this.getAuthor();
  }

  private getAuthor(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') !== 'new') {
          return this.authorService.getAuthor(+params.get('id'));
        } else {
          return this.authorService.getNewAuthor();
        }
      })
      .subscribe(author => this.author = author);
  }
  public fileChange(event): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      this.authorService.uploadFile(formData, this.author)
        .then(img_url => this.author.img_url = img_url);
    }
  }
  public save(event): void {
    if (this.author.id === 0) {
      this.authorService.create(this.author).then(() => this.router.navigate(['/authors']));
    } else {
      this.authorService.update(this.author).then(() => this.router.navigate(['/authors']));
    }
  }
}
