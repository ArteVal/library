import { Component, OnInit } from '@angular/core';

import { AuthorService} from '../../services/author.service';
import { Author } from '../../interfaces/author';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {
  public totalItems: number;
  public currentPage = 1;
  public perPage = 5;
  public authors: Author[];
  constructor( private authorService: AuthorService) { }
  
  ngOnInit() {
    this.getAuthorList();
  }
  
  private getAuthorList(): void {
    this.authorService.getAuthors(this.perPage, 1)
      .then(authors => {
        this.authors = authors;
        console.log(authors);
      });
  }
  public setPage(pageNo: number): void {
  }
  public pageChanged(event: any): void {
    this.authorService.getAuthors(this.perPage, event.page);
  }
}
