import { Component, OnInit } from '@angular/core';
import { BookService} from '../../services/book.service';
import { Book } from '../../interfaces/book';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  public book: Book;
  constructor(
    private bookService: BookService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    this.getBook();
  }
  private getBook(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') !== 'new') {
          return this.bookService.getBook(+params.get('id'));
        } else {
          return this.bookService.getNewBook();
        }
      })
      .subscribe(author => this.book = author);
  }
  public fileChange(event): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      this.bookService.uploadFile(formData, this.book)
        .then(img_url => this.book.img_url = img_url);
    }
  }
  public save(event): void {
    if (this.book.id === 0) {
      this.bookService.create(this.book).then(() => this.router.navigate(['/books']));
    } else {
      this.bookService.update(this.book).then(() => this.router.navigate(['/books']));
    }
  }
}
