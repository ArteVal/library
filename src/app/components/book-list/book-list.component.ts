import { Component, OnInit } from '@angular/core';

import { BookService} from '../../services/book.service';
import { Book } from '../../interfaces/book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  public totalItems: number;
  public currentPage = 1;
  public perPage = 5;
  public books: Book[];
  
  constructor( private bookService: BookService) { }
  
  ngOnInit() {
    this.getBooksList();
  }
  
  private getBooksList(): void {
    this.bookService.getBooks(this.perPage, 1)
      .then(books => this.books = books);
  }
  public setPage(pageNo: number): void {
  }
  public pageChanged(event: any): void {
    this.bookService.getBooks(this.perPage, event.page);
  }
}
