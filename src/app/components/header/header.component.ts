import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public user: User;
  public isUserLogin = false;
  public userLogin: string;
  public password: string;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.user.subscribe(user => this.user = user);
    this.userService.isLogin.subscribe(isLogin => this.isUserLogin = isLogin);
  }
  login(): void {
    this.userService.login({
      name: this.userLogin,
      pass: this.password}
    );
  }
  logout(): void {
    this.userService.logout();
  }
}
