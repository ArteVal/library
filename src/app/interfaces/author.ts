import { Book } from './book';

export interface Author {
  id: number;
  name: string;
  descr: string;
  img_url: string;
  img_file?: any;
  books?: Book[];
}
