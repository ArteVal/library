import { Author } from './author';
export interface Book {
  id: number;
  title: string;
  author?: Author;
  descr: string;
  pages: number;
  year: number;
  img_url: string;
  img_file?: any;
}
