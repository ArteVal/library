export interface User {
  name: string;
  pass: string;
  role: string;
}
