import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Author } from '../interfaces/author';
@Injectable()
export class AuthorService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:3000/author';  // URL to web api
  constructor (private http: Http) {}

  public getAuthors(perPage: number, page: number): Promise<Author[]> {
    // переделать под пагинацию
    return this.http.get(this.url + '/list')
      .toPromise()
      .then(response => response.json().rows as Author[])
      .catch(this.handleError);
  }
  public getAuthor(id: number): Promise<Author> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Author)
      .catch(this.handleError);
  }
  public getNewAuthor(): Promise<Author> {
    const author: Author = {
      id: 0,
      name: '',
      descr: '',
      img_url: ''
    };
    return Promise.resolve(author);
  }
  public create(author: Author): Promise<Author> {
    return this.http
      .post(this.url, JSON.stringify(author), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Author)
      .catch(this.handleError);
  }

  public update(author: Author): Promise<Author> {
    const url = `${this.url}/${author.id}`;
    return this.http
      .put(url, JSON.stringify(author), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Author)
      .catch(this.handleError);
  }

  public delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  public uploadFile(formData: FormData, author: Author): Promise<string> {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return this.http
      .post(`${this.url}/${author.id}/img`, formData, { headers: headers })
      .toPromise()
      .then(responce => responce.json().file as string)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}

