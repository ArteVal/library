import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Book } from '../interfaces/book';

@Injectable()
export class BookService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:3000/book';  // URL to web api

  constructor (private http: Http) {}
  public getBooks(perPage: number, page: number): Promise<Book[]> {
    // переделать под пагинацию
    return this.http.get(this.url + '/list')
      .toPromise()
      .then(response => response.json().rows as Book[])
      .catch(this.handleError);
  }
  public getBooksCount(): Promise<number> {
    return Promise.resolve(0);
  }
  public getBook(id: number): Promise<Book> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Book)
      .catch(this.handleError);
  }
  public getNewBook(): Promise<Book> {
    const Book: Book = {
      id: 0,
      title: '',
      descr: '',
      pages: 0,
      year: 2017,
      img_url: ''
    };
    return Promise.resolve(Book);
  }
  public create(Book: Book): Promise<Book> {
    return this.http
      .post(this.url, JSON.stringify(Book), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Book)
      .catch(this.handleError);
  }

  public update(Book: Book): Promise<Book> {
    const url = `${this.url}/${Book.id}`;
    return this.http
      .put(url, JSON.stringify(Book), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Book)
      .catch(this.handleError);
  }

  public delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  public uploadFile(formData: FormData, Book: Book): Promise<string> {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return this.http
      .post(`${this.url}/${Book.id}/img`, formData, { headers: headers })
      .toPromise()
      .then(responce => responce.json().file as string)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
