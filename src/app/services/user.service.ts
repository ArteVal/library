import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { User } from '../interfaces/user';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class UserService {

  private userSource = new Subject<User>();
  private userisLogin = new Subject<boolean>();
  public user = this.userSource.asObservable();
  public isLogin = this.userisLogin.asObservable();

  constructor (private http: Http) {}

  login(user: {name: string, pass: string}): void {
    console.log(user);
    this.http.post('http://localhost:3000/login', user).toPromise().then(response => {
      console.log(response);
      const resp = response.json();
      if (resp.isLogined === 1) {
        this.userisLogin.next(true);
        this.userSource.next(resp.user);
      }
    });
  }
  logout(): void {
    this.userisLogin.next(false);
  }
}
